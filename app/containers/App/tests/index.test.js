import React from 'react';
import { shallow, mount } from 'enzyme';
import { Route, MemoryRouter } from 'react-router-dom';

import App from '../index';
import HomePage from '../../HomePage/index';
import NotFoundPage from '../../NotFoundPage/index';

describe('<App />', () => {
  it('should render some routes', () => {
    const renderedComponent = shallow(<App />);
    expect(renderedComponent.find(Route).length).not.toBe(0);
  });

  test('invalid path should redirect to 404', () => {
    const wrapper = shallow(<App />);

    expect(
      wrapper
        .find('Route[exact=true][path="/"]')
        .first()
        .prop('component'),
    ).toBe(HomePage);
  });
});
